#!/usr/bin/env bash

set -x -e -o pipefail

# Copy docker-compose file to server.
rsync -avz -e "ssh" $SYNC_FOLDER/ $REMOTE_USER@$REMOTE_HOST:~

# deploy
ssh -t $REMOTE_USER@$REMOTE_HOST << EOF

set -x -e -o pipefail

cd ~
# login in registry
echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
# pull images
docker-compose -f docker-compose.yml pull
docker logout $CI_REGISTRY

docker-compose -f docker-compose.yml down;

# start app
docker-compose -f docker-compose.yml up -d

EOF
