import React from 'react';
import './App.css';
import Cookies from "universal-cookie";
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css";
import './page/main-content.css'
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Header from './components/header/Header'
import LoginPage from "./page/login/LoginPage";
import SignUpPage from "./page/login/SignUpPage";
import NotFoundPage from "./page/NotFoundPage";
import Dashboard from "./page/Dashboard";
import ProductsPage from "./page/ProductsPage";
import RecipesPage from "./page/RecipesPage";
import RationsPage from "./page/RationsPage";
import SettingsPage from "./page/SettingsPage";
import RecipePage from "./page/RecipePage";

const RouteAuthorized = ({exact, path, component: Component}) => (
    <Route exact={exact} path={path} render={(props) => (
        isAuthorized() ? (
            <div className='box'>
              <Header/>
              <div className='content'>
                <Component {...props}/>
              </div>
            </div>
        ) : (
            <Redirect
                to={{
                  pathname: "/login"
                }}
            />
        )
    )}/>
);

const RouteOnlyNotAuthorized = ({exact, path, component: Component}) => (
    <Route exact={exact} path={path} render={(props) => (
        !isAuthorized() ? (
            <div className='content'>
              <Component {...props}/>
            </div>
        ) : (
            <Redirect
                to={{
                  pathname: "/"
                }}
            />
        )
    )}/>
);

const RouteNotAuthorized = ({exact, path, component: Component}) => (
    <Route exact={exact} path={path} render={(props) => (
        <div className='content'>
          <Component {...props}/>
        </div>
    )}/>
);

export default function App() {
  return <div className="App box">
    <Router>
      <Switch>
        <RouteOnlyNotAuthorized exact component={LoginPage} path='/login'/>
        <RouteAuthorized exact component={Dashboard} path="/"/>
        <RouteAuthorized exact component={ProductsPage} path="/products"/>
        <RouteAuthorized exact component={RecipesPage} path="/recipes"/>
        <RouteAuthorized exact component={RecipePage} path="/recipe/:id"/>
        <RouteAuthorized exact component={RationsPage} path="/rations"/>
        <RouteAuthorized exact component={SettingsPage} path="/settings"/>
        <RouteOnlyNotAuthorized exact component={SignUpPage} path='/sign-up'/>
        <RouteNotAuthorized component={NotFoundPage}/>
      </Switch>
    </Router>
  </div>
}

function isAuthorized() {
  return !!new Cookies().get('authToken')
}