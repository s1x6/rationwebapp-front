import {BACKEND_URL} from "./constants";
import Cookies from "universal-cookie";

export function fetchPost(path, body) {
    let responseStatus;
    return fetch(BACKEND_URL + path, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + new Cookies().get('authToken')
        },
        body: body
    }).then((response) => {
        responseStatus = response.status;
        return response.json()
    }).then((responseJson) => {
        return {
            status: responseStatus,
            responseJson: responseJson
        }
    })
}

export function fetchPostFormData(path, data) {
    let responseStatus;
    return fetch(BACKEND_URL + path, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + new Cookies().get('authToken')
        },
        body: data
    }).then((response) => {
        responseStatus = response.status;
        return response.json()
    }).then((responseJson) => {
        return {
            status: responseStatus,
            responseJson: responseJson
        }
    })
}

export function fetchGet(path) {
    let responseStatus;
    return fetch(BACKEND_URL + path, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + new Cookies().get('authToken')
        },
    }).then((response) => {
        responseStatus = response.status;
        return response.json()
    }).then((responseJson) => {
        return {
            status: responseStatus,
            responseJson: responseJson
        }
    })
}

export function fetchDelete(path) {
    let responseStatus;
    return fetch(BACKEND_URL + path, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + new Cookies().get('authToken')
        },
    }).then((response) => {
        responseStatus = response.status;
        return response.json()
    }).then((responseJson) => {
        return {
            status: responseStatus,
            responseJson: responseJson
        }
    })
}

export function fetchLogin(path, body) {
    let responseStatus;
    return fetch(BACKEND_URL + path, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        credentials: 'same-origin', // <- this is mandatory to deal with cookies
        body: body
    }).then((response) => {
        responseStatus = response.status;
        return response.json()
    }).then((responseJson) => {
        return {
            status: responseStatus,
            responseJson: responseJson
        }
    })
}