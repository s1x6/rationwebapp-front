import React, {useEffect, useState} from "react";
import {Button, Card, CardBody, CardTitle} from "shards-react";
import DataTable from "react-data-table-component";
import {fetchGet} from "../../utils";
import RecipeCreate from "./RecipeCreate";
import {useHistory} from "react-router";

export default function (props) {

    const history = useHistory()
    const [data, setData] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [totalRows, setTotalRows] = useState(0)
    const [page, setPage] = useState(1)
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [isCreateShow, setIsCreateShow] = useState(false)

    useEffect(() => {
        load(0, 0)
    }, [])

    const columns = [
        {
            name: 'Название',
            selector: 'name',
            sortable: true
        },
        {
            name: 'Калории (100г)',
            selector: 'calories',
            sortable: true
        },
        {
            name: 'Белки (100г)',
            selector: 'proteins',
            sortable: true
        },
        {
            name: 'Жиры (100г)',
            selector: 'fats',
            sortable: true
        },
        {
            name: 'Углеводы (100г)',
            selector: 'carbohydrates',
            sortable: true
        }
    ]
    const handleCreateClose = () => {
        setIsCreateShow(false)
    }

    function load(page, rowsPerPage) {
        fetchGet('/recipe/latest').then(({responseJson, status}) => {
            if (status === 200) {
                setData(responseJson.data)
            } else {
                alert('status:' + status)
            }
        })
            .catch((reason => alert(reason)))
            .finally(() => {
                setIsLoading(false)
            })
    }

    const onChangePage = (page) => {
        setIsLoading(true)
        setPage(page)
        load(page, rowsPerPage)
    }

    const onChangeRowsPerPage = (rows) => {
        setIsLoading(true)
        setRowsPerPage(rows)
        load(page, rows)
    }

    const onRowClick = (row) => {
        history.push('/recipe/' + row.id)
    }

    const onCreateClicked = () => {
        setIsCreateShow(true)
    }
    return <div>
        <Card style={{marginTop: '16px', width: "100%"}}>
            <CardBody>
                <CardTitle>Последние рецепты</CardTitle>
                <DataTable
                    columns={columns}
                    data={data}
                    dense
                    direction={"ltr"}
                    actions={<Button outline onClick={onCreateClicked}>
                        Создать
                    </Button>}
                    progressPending={isLoading}
                    pagination
                    paginationServer
                    striped={true}
                    onChangePage={onChangePage}
                    onChangeRowsPerPage={onChangeRowsPerPage}
                    paginationTotalRows={totalRows}
                    highlightOnHover={true}
                    progressComponent={<h4>Загрузка...</h4>}
                    noDataComponent={<div>Рецепты не найдены</div>}
                    onRowClicked={onRowClick}
                />
                <RecipeCreate show={isCreateShow} handleClose={handleCreateClose}/>
            </CardBody>
        </Card>
    </div>
}