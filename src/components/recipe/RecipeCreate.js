import React, {useEffect, useState} from "react";
import {Table, ToggleButtonGroup, Button, FormControl, InputGroup, Modal, Spinner} from "react-bootstrap";
import {fetchGet, fetchPost, fetchPostFormData} from "../../utils";
import {ToggleButton} from "react-bootstrap";
import {Col, Container, Form, Row} from "shards-react";
import SearchAutocomplete from "../SearchAutocomplete";
import ProductWeightSelectModal from "./ProductWeightSelectModal";

export default function (props) {

    const [name, setName] = useState('')
    const [desc, setDesc] = useState('')
    const [totalCl, setTotalCl] = useState(0)
    const [totalCb, setTotalCb] = useState(0)
    const [totalP, setTotalP] = useState(0)
    const [totalF, setTotalF] = useState(0)
    const [totalW, setTotalW] = useState(0)
    const [image, setImage] = useState()
    const [portionSize, setPortionSize] = useState(100)
    const [isSaving, setIsSaving] = useState(false)
    const [weightShow, setWeightShow] = useState(false)
    const [products, setProducts] = useState([])
    const [tags, setTags] = useState([])
    const [selectedTags, setSelectedTags] = useState([])
    const [selectedProduct, setSelectedProduct] = useState({})
    const [steps, setSteps] = useState([{num: 1, value: ''}])

    useEffect(() => {
        fetchGet('/recipe/tag').then(({responseJson, status}) => {
            if (status === 200) {
                if (responseJson.status === 'OK') {
                    setTags(responseJson.data)
                } else {
                    alert(responseJson.errorText)
                }
            } else {
                alert('status:' + status)
            }
        }).catch((reason => alert(reason)))

    }, [])

    const onSaveClick = () => {
        setIsSaving(true)
        const today = new Date();
        const imageName = 'recipe' + today.getHours() + '' + today.getMinutes() + '' + today.getSeconds();
        const formData = new FormData();
        formData.append('file', image);
        formData.append('name', imageName);
        // todo handle errors
        fetchPostFormData('/recipe/image', formData)
        fetchPost('/recipe', JSON.stringify({
            name: name,
            description: desc,
            portionSize: portionSize,
            tags: selectedTags,
            imageName: imageName,
            steps: steps.map((s) => {
                return {num: s.num, text: s.value}
            }),
            ingredients: products.map((p) => {
                return {
                    productId: p.p.id,
                    name: p.p.name,
                    amount: p.w
                }
            })
        })).then(({responseJson, status}) => {
            if (status === 200) {
                if (responseJson.status === 'OK') {
                    props.handleClose()
                } else if (responseJson.status === 'ERROR') {
                    alert('error: ' + responseJson.errorText)
                }
            } else {
                alert('status: ' + status)
            }
        }).catch((reason => alert(reason)))
            .finally(() => {
                setIsSaving(false)
            })
    }

    const onTagSelected = (e) => {
        setSelectedTags(e)
    }

    const renderTagsButtons = () => {
        let arr = [];
        tags.forEach((e) => {
            arr = [...arr, (<ToggleButton key={e.id} value={e}>{e.name}</ToggleButton>)]
        })
        return arr
    }

    const renderProducts = () => {
        let arr = [];
        products.forEach((e) => {
                arr = [...arr, <tr>
                    <th>{e.p.name}</th>
                    <th>{e.p.calories}</th>
                    <th>{e.p.carbohydrates}</th>
                    <th>{e.p.proteins}</th>
                    <th>{e.p.fats}</th>
                    <th>{e.w}</th>
                </tr>]
            }
        )
        return arr
    }

    const onAddStepClick = (e) => {
        if (steps[steps.length - 1].value !== '') {
            setSteps([...steps, {num: steps.length + 1, value: ''}])
        }
    }

    const onProductSelect = (p) => {
        setSelectedProduct(p)
        setWeightShow(true)
    }

    const onFileSelected = (e) => {
        e.preventDefault()
        setImage(e.target.files[0])
    }

    const onWeightSet = (w) => {
        setProducts([...products, {p: selectedProduct, w: w}])
        setTotalW(totalW + w)
        setTotalCl(totalCl + selectedProduct.calories * w / 100)
        setTotalCb(totalCb + selectedProduct.carbohydrates * w / 100)
        setTotalF(totalF + selectedProduct.fats * w / 100)
        setTotalP(totalP + selectedProduct.proteins * w / 100)
    }

    const weightHandleClose = () => {
        setWeightShow(false)
    }

    const onHide = () => {
        setTotalW(0)
        setTotalCl(0)
        setTotalCb(0)
        setTotalF(0)
        setTotalP(0)
        setName('')
        setDesc('')
        setProducts([])
        setPortionSize(100)
        setSteps([{num: 1, value: ''}])
        props.handleClose()
    }

    const IngredientsList = () => <Table style={{width: '100%'}} striped hover>
        <thead>
        <tr>
            <th>Название</th>
            <th>К</th>
            <th>У</th>
            <th>Б</th>
            <th>Ж</th>
            <th>В</th>
        </tr>
        </thead>
        <tbody>
        {renderProducts()}
        </tbody>
    </Table>

    return <Modal show={props.show} size='lg' onHide={onHide}>
        <Modal.Header closeButton>
            <Modal.Title>Добавление нового рецепта</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Row>
                    <Col>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>Название</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                onChange={(e) => setName(e.target.value)}
                                value={name}/>
                        </InputGroup>
                    </Col>
                    <Col>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>Размер порции</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                onChange={(e) => setPortionSize(parseInt(e.target.value))}
                                value={portionSize}/>
                        </InputGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text>Описание</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                onChange={(e) => setDesc(e.target.value)}
                                value={desc}/>
                        </InputGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ToggleButtonGroup type="checkbox" value={selectedTags} onChange={onTagSelected}>
                            {renderTagsButtons()}
                        </ToggleButtonGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <input type='file' onChange={onFileSelected}/>
                    </Col>
                </Row>
                <div>
                    {steps.map((e) => <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>{'Шаг ' + e.num}</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            onChange={(ev) => {
                                const copy = JSON.parse(JSON.stringify(steps))
                                copy[e.num - 1].value = ev.target.value
                                setSteps(copy)
                            }}
                            value={e.value}/>
                    </InputGroup>)}
                    <Button onClick={onAddStepClick}>Добавить шаг</Button>
                </div>
                <Container style={{marginTop: '10px'}} className="dr-example-container">
                    <Row>
                        <Col>Общее:</Col>
                        <Col>{"Калории: " + totalCl.toFixed(2)}</Col>
                        <Col>{"Углеводы: " + totalCb.toFixed(2)}</Col>
                        <Col>{"Белки: " + totalP.toFixed(2)}</Col>
                        <Col>{"Жиры: " + totalF.toFixed(2)}</Col>
                    </Row>
                    <Row>
                        <Col>На 100 грамм:</Col>
                        <Col>{"Калории: " + (totalW > 0 ? totalCl / totalW * 100 : 0).toFixed(2)}</Col>
                        <Col>{"Углеводы: " + (totalW > 0 ? totalCb / totalW * 100 : 0).toFixed(2)}</Col>
                        <Col>{"Белки: " + (totalW > 0 ? totalP / totalW * 100 : 0).toFixed(2)}</Col>
                        <Col>{"Жиры: " + (totalW > 0 ? totalF / totalW * 100 : 0).toFixed(2)}</Col>
                    </Row>
                    <Row>
                        <Col>На 1 порцию:</Col>
                        <Col>{"Калории: " + (totalW > 0 ? totalCl / totalW * portionSize : 0).toFixed(2)}</Col>
                        <Col>{"Углеводы: " + (totalW > 0 ? totalCb / totalW * portionSize : 0).toFixed(2)}</Col>
                        <Col>{"Белки: " + (totalW > 0 ? totalP / totalW * portionSize : 0).toFixed(2)}</Col>
                        <Col>{"Жиры: " + (totalW > 0 ? totalF / totalW * portionSize : 0).toFixed(2)}</Col>
                    </Row>
                </Container>
                <hr/>
                <h4>Ингредиенты</h4>
                <table>
                    <tbody>
                    <tr>
                        <td style={{width: '50%'}}>
                            <SearchAutocomplete onSelect={onProductSelect}/>
                        </td>
                        <td style={{width: '50%'}}>
                            <IngredientsList/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <ProductWeightSelectModal
                    show={weightShow}
                    handleClose={weightHandleClose}
                    onWeightSelected={onWeightSet}
                />
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={props.handleClose}>
                Закрыть
            </Button>
            <Button variant="primary" onClick={onSaveClick}>
                {isSaving ? (<Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />) : "Сохранить"}
            </Button>
        </Modal.Footer>
    </Modal>
}