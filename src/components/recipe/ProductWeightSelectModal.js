import React, {useState} from "react";
import {Button, FormControl, InputGroup, Modal} from "react-bootstrap";

export default function (props) {

    const [weight, setWeight] = useState(0)

    const onSaveClick = () => {
        props.onWeightSelected(weight)
        props.handleClose()
        setWeight(0)
    }

    return <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Укажите количество</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Количество (грамм)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setWeight(parseInt(e.target.value))}
                    value={weight}/>
            </InputGroup>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={props.handleClose}>
                Закрыть
            </Button>
            <Button variant="primary" onClick={onSaveClick}>
                 Сохранить
            </Button>
        </Modal.Footer>
    </Modal>
}