import React, {useState} from "react";
import {Button, FormControl, InputGroup, Modal} from "react-bootstrap";
import SearchAutocomplete from "../SearchAutocomplete";
import {Col, Container, Row} from "shards-react";

export const recipeDeepCopy = (r) => {
    const copy = Object.assign({}, r)
    copy.nutrientsDefault = Object.assign({}, r.nutrientsDefault)
    copy.nutrientsTotal = Object.assign({}, r.nutrientsTotal)
    copy.nutrientsPortion = Object.assign({}, r.nutrientsPortion)
    copy.tags = r.tags.map((t) => Object.assign({}, t))
    copy.steps = r.steps.map((t) => Object.assign({}, t))
    copy.ingredients = r.ingredients.map((t) => Object.assign({}, t))
    return copy
}

export function StepsEdit(props) {

    const setSteps = (e) => {
        const copy = recipeDeepCopy(props.recipe)
        copy.steps = e
        props.setRecipe(copy)
    }

    const onAddStepClick = () => {
        if (props.recipe.steps[props.recipe.steps.length - 1].value !== '') {
            setSteps([...props.recipe.steps, {num: props.recipe.steps.length + 1, value: ''}])
        }
    }

    const onDeleteStepClick = (num) => {
        const copy = [...props.recipe.steps]
        copy.splice(num - 1, 1)
        for (let i = num - 1; i < copy.length; i++) {
            copy[i].num = i + 1
        }
        setSteps(copy)
    }

    return <>{props.recipe.steps.map((e) => <InputGroup className="mb-3">
        <InputGroup.Prepend>
            <InputGroup.Text>{'Шаг ' + e.num}</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
            onChange={(ev) => {
                const copy = JSON.parse(JSON.stringify(props.recipe.steps))
                copy[e.num - 1].value = ev.target.value
                setSteps(copy)
            }}
            value={e.text}/>
        <InputGroup.Append>
            <Button variant="danger" onClick={() => onDeleteStepClick(e.num)}>-</Button>
        </InputGroup.Append>
    </InputGroup>)}
        <Button onClick={onAddStepClick}>Добавить шаг</Button>
    </>
}

export function RecipeAddIngredient(props) {

    const [ingredient, setIngredient] = useState(undefined)
    const [amount, setAmount] = useState(100)

    const onHide = () => {
        setIngredient(undefined)
        setAmount(100)
        props.onHide()
    }
    const onIngredientSelect = (e) => {
        setIngredient(e)
    }
    const onSaveClick = () => {
        props.onAdd({...ingredient, amount: amount, productId: ingredient.id})
        onHide()
    }

    return <Modal show={props.show} onHide={onHide}>
        <Modal.Header closeButton>
            <Modal.Title>Добавление ингредиента</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Описание</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setAmount(e.target.value)}
                    value={amount}/>
            </InputGroup>
            {ingredient === undefined ? <SearchAutocomplete onSelect={onIngredientSelect}/> :
                <Container>
                    <Row style={{marginTop: '8px'}}>
                        <Col><b>{ingredient.name}</b></Col>
                        <Col><b>К: </b><i>{ingredient.calories.toFixed(1)}</i></Col>
                        <Col><b>У: </b><i>{ingredient.carbohydrates.toFixed(1)}</i></Col>
                        <Col><b>Б: </b><i>{ingredient.proteins.toFixed(1)}</i></Col>
                        <Col><b>Ж: </b><i>{ingredient.fats.toFixed(1)}</i></Col>
                    </Row>
                </Container>}
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>
                Закрыть
            </Button>
            <Button disabled={ingredient === undefined || amount < 1}
                    variant="primary" onClick={onSaveClick}>
                Сохранить
            </Button>
        </Modal.Footer>
    </Modal>
}