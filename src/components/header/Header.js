import React from 'react';
import logo from '../../logo.svg';
import './header.css'
import {Link} from "react-router-dom";
import {Nav, NavItem} from "shards-react";

export default function () {
    return <Nav className='Header header'>
        <NavItem>
            <Link className='nav-link' style={{padding: '0'}} to='/'>
                <img alt='logo' style={{marginLeft: '4px', width: '30px', height: '30px'}} src={logo}/>
            </Link>
        </NavItem>
        <NavItem>
            <Link className='nav-link' to='/'>
                Главная
            </Link>
        </NavItem>
        <NavItem>
            <Link className='nav-link' to='/products'>
                Продукты
            </Link>
        </NavItem>
        <NavItem>
            <Link className='nav-link' to='/recipes'>
                Рецепты
            </Link>
        </NavItem>
        <NavItem>
            <Link className='nav-link' to='/rations'>
                Рационы
            </Link>
        </NavItem>
        <NavItem>
            <Link className='nav-link' to='/settings'>
                Настройки
            </Link>
        </NavItem>
    </Nav>
}