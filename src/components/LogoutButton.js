import React from 'react'
import {Button} from "shards-react";
import Cookies from "universal-cookie";
import {useHistory} from "react-router-dom";

export default function LogoutButton() {

   let history = useHistory();

    function onLogout() {
        const cookies = new Cookies();
        cookies.remove("authToken");
        history.push("/login");
    }

    return (
        <Button theme='danger' onClick={onLogout}>
            Выйти
        </Button>
    )
}