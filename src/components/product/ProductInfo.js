import React, {useEffect, useState} from "react";
import {Button, FormControl, Modal, InputGroup, Spinner} from "react-bootstrap";
import {FormCheckbox} from "shards-react";
import './info.css'
import {fetchPost} from "../../utils";

export default function (props) {

    const [isEditing, setIsEditing] = useState(false)
    const [isSaving, setIsSaving] = useState(false)
    const [currentProduct, setCurrentProduct] = useState({portionInfo: {}})

    const onClose = () => {
        setIsEditing(false)
        props.handleClose()
    }

    useEffect(() => {
        if (props.product === undefined) return;
        const p = Object.assign({}, props.product)
        p.portionInfo = Object.assign({}, props.product.portionInfo)
        setCurrentProduct(p)
    }, [props.product])

    const getDeepCopyOfProduct = () => {
        const p = Object.assign({}, currentProduct)
        p.portionInfo = Object.assign({}, props.product.portionInfo)
        return p
    }

    const onSaveClick = () => {
        setIsSaving(true)
        fetchPost('/product', JSON.stringify(currentProduct))
            .then(({responseJson, status}) => {
                if (status === 200) {
                    if (responseJson.status === 'OK') {
                        props.setProduct(responseJson.data)
                        setIsEditing(false)
                    } else if (responseJson.status === 'ERROR') {
                        alert('Ошибка: ' + responseJson.errorText)
                    }
                } else {
                    alert('Статус: ' + status)
                }
            })
            .catch((reason => alert(reason)))
            .finally(() => {
                setIsSaving(false)
            })
    }

    return <Modal show={props.show} size='lg' onHide={onClose}>
        <Modal.Header closeButton>
            <div>
                <Modal.Title>Информация о продукте</Modal.Title>
                {
                    currentProduct.isSystem ?
                        <div>Вы не можете редактировать продукты, добавленные не Вами</div> :
                        <FormCheckbox
                            checked={isEditing}
                            toggle
                            small
                            onChange={() => {
                                setIsEditing(!isEditing)
                            }}>Редактирование</FormCheckbox>
                }
            </div>
        </Modal.Header>
        <Modal.Body>
            {
                props.isLoading ? <div>
                    Загрузка...
                </div> : <div>
                    <h5>Общее</h5>
                    <table>
                        <tbody>
                        <tr>
                            <td colSpan={3}>
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Название</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        readOnly={!isEditing}
                                        onChange={(e) => {
                                            const p = getDeepCopyOfProduct()
                                            p.name = e.target.value
                                            setCurrentProduct(p)
                                        }}
                                        value={isEditing ? currentProduct.name : props.product.name}/>
                                </InputGroup>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Калории (100г)</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        readOnly={!isEditing}
                                        onChange={(e) => {
                                            const p = getDeepCopyOfProduct()
                                            p.calories = e.target.value
                                            setCurrentProduct(p)
                                        }}
                                        value={isEditing ? currentProduct.calories : props.product.calories}/>

                                </InputGroup>
                            </td>
                            <td>
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Углеводы (100г)</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        readOnly={!isEditing}
                                        onChange={(e) => {
                                            const p = getDeepCopyOfProduct()
                                            p.carbohydrates = e.target.value
                                            setCurrentProduct(p)
                                        }}
                                        value={isEditing ? currentProduct.carbohydrates : props.product.carbohydrates}/>

                                </InputGroup>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Белки (100г)</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        readOnly={!isEditing}
                                        onChange={(e) => {
                                            const p = getDeepCopyOfProduct()
                                            p.proteins = e.target.value
                                            setCurrentProduct(p)
                                        }}
                                        value={isEditing ? currentProduct.proteins : props.product.proteins}/>

                                </InputGroup>
                            </td>
                            <td>
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Жиры (100г)</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        readOnly={!isEditing}
                                        onChange={(e) => {
                                            const p = getDeepCopyOfProduct()
                                            p.fats = e.target.value
                                            setCurrentProduct(p)
                                        }}
                                        value={isEditing ? currentProduct.fats : props.product.fats}/>
                                </InputGroup>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <hr/>
                    <h5>Порция</h5>
                    <table>
                        <tbody>
                        <tr>
                            <td colSpan={3}>
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Порция (г)</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        readOnly={!isEditing}
                                        onChange={(e) => {
                                            const p = getDeepCopyOfProduct()
                                            p.portionInfo.size = e.target.value
                                            setCurrentProduct(p)
                                        }}
                                        value={isEditing ? currentProduct.portionInfo.size :
                                            (props.product.portionInfo ? props.product.portionInfo.size : 0)}/>
                                </InputGroup>
                            </td>
                        </tr>
                        {!isEditing ? <tr>
                                <td>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>Калории</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl
                                            readOnly
                                            value={currentProduct.portionInfo.calories}/>
                                    </InputGroup>
                                </td>
                                <td>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>Углеводы</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl
                                            readOnly
                                            value={currentProduct.portionInfo.carbohydrates}/>
                                    </InputGroup>
                                </td>
                            </tr> : null }
                            {!isEditing ? <tr>
                                <td>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>Белки</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl
                                            readOnly
                                            value={currentProduct.portionInfo.proteins}/>
                                    </InputGroup>
                                </td>
                                <td>
                                    <InputGroup className="mb-3">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>Жиры</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl
                                            readOnly
                                            value={currentProduct.portionInfo.fats}/>
                                    </InputGroup>
                                </td>
                            </tr> : null}
                        </tbody>
                    </table>
                </div>
            }
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onClose}>
                Закрыть
            </Button>
            {isEditing ? <Button variant="primary" onClick={onSaveClick}>
                {isSaving ? (<Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />) : "Сохранить"}
            </Button> : null
            }
        </Modal.Footer>
    </Modal>
}