import React, {useState} from "react";
import {Button, FormControl, InputGroup, Modal, Spinner} from "react-bootstrap";
import {fetchPost} from "../../utils";

export default function (props) {

    const [isSaving, setIsSaving] = useState(false)
    const [name, setName] = useState('')
    const [calories, setCalories] = useState(0.0)
    const [carbohydrates, setCarbohydrates] = useState(0.0)
    const [fats, setFats] = useState(0.0)
    const [proteins, setProteins] = useState(0.0)
    const [portionSize, setPortionSize] = useState(100)

    const onSaveClick = () => {
        setIsSaving(true)
        fetchPost('/product', JSON.stringify({
            id: 0,
            name: name,
            calories: calories,
            carbohydrates: carbohydrates,
            fats: fats,
            proteins: proteins,
            portionInfo: {
                size: portionSize
            }
        }))
            .then(({responseJson, status}) => {
                if (status === 200) {
                    if (responseJson.status === 'OK') {
                        props.onCreate()
                        handleClose()
                    } else if (responseJson.status === 'ERROR') {
                        alert('Ошибка: ' + responseJson.errorText)
                    }
                } else {
                    alert('Статус: ' + status)
                }
            })
            .catch((reason => alert(reason)))
            .finally(() => {
                setIsSaving(false)
            })
    }

    const handleClose = () => {
        setPortionSize(100)
        setCarbohydrates(0.0)
        setName('')
        setCalories(0.0)
        setProteins(0.0)
        setFats(0.0)
        props.handleClose()
    }

    return <Modal show={props.show} size='lg' onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Добавление нового продукта</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Название</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setName(e.target.value)}
                    value={name}/>
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Калории (100г)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setCalories(parseFloat(e.target.value))}
                    value={calories}/>
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Углеводы (100г)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setCarbohydrates(parseFloat(e.target.value))}
                    value={carbohydrates}/>
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Белки (100г)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setProteins(parseFloat(e.target.value))}
                    value={proteins}/>
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Жиры (100г)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setFats(parseFloat(e.target.value))}
                    value={fats}/>
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Размер порции</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    onChange={(e) => setPortionSize(parseInt(e.target.value))}
                    value={portionSize}/>
            </InputGroup>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
                Закрыть
            </Button>
            <Button variant="primary" onClick={onSaveClick}>
                {isSaving ? (<Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />) : "Сохранить"}
            </Button>
        </Modal.Footer>
    </Modal>
}