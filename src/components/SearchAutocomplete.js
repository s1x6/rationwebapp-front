import React, {useState} from "react";
import {fetchGet} from "../utils";
import {ListGroup, Table} from "react-bootstrap";
import {Button, Card, CardBody} from "shards-react";

export default function (props) {
    const [results, setResults] = useState([])
    const [timeoutId, setTimeoutId] = useState(null)
    const [q, setQ] = useState('')

    const getInfo = (q) => {
        fetchGet('/product/search?q=' + q)
            .then(({responseJson, status}) => {
                if (status === 200) {
                    if (responseJson.status === 'OK') {
                        setResults(responseJson.data)
                    } else if (responseJson.status === 'ERROR') {
                        alert('error: ' + responseJson.errorText)
                    }
                } else {
                    alert('status: ' + status)
                }
            }).catch((reason => alert(reason)))
    }

    const handleInputChange = (e) => {
        setQ(e.target.value)
        if (e.target.value.length > 1) {
            if (timeoutId !== null) {
                clearTimeout(timeoutId)
            }
            setTimeoutId(setTimeout((t) => getInfo(t), 800, [e.target.value]))
        }
        if (e.target.value.length === 0) {
            setResults([])
        }
    }
    return (
        <form>
            <input
                placeholder="Название ингредиента..."
                onChange={handleInputChange}
                value={q}
            />
            <Suggestions results={results} onClick={(r) => {
                setResults([])
                setQ('')
                props.onSelect(r)
            }}/>
        </form>
    )
}

const Suggestions = (props) => {
    const options = props.results.map(r => (
        <ListGroup.Item key={r.id} >
            {r.name}
        </ListGroup.Item>
    ))
    return (props.results.length > 0 ?<Card style={{maxWidth: '600px', zIndex: '9999', position: 'absolute'}}>
        <CardBody>
            <Table size='sm' style={{width: '100%'}} striped hover>
                <tbody>
                {props.results.map((e) => <>
                    <tr onClick={() => props.onClick(e)}>
                        <th>{e.name}</th>
                        <th>K: {e.calories}</th>
                        <th>У: {e.carbohydrates}</th>
                        <th>Б: {e.proteins}</th>
                        <th>Ж: {e.fats}</th>
                    </tr>
                </>)}
                </tbody>
            </Table>
        </CardBody>
    </Card> : null)
}