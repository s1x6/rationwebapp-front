import React, {useEffect, useState} from "react";
import {fetchGet} from "../utils";
import {Button, Card} from "react-bootstrap";
import {BACKEND_URL} from "../constants";
import {useHistory} from "react-router";

export default function (props) {

    const history = useHistory()

    const [randomRecipes, setRandomRecipes] = useState([])

    useEffect(() => {
        document.title = " Главная - Ration"
        fetchGet('/recipe/random').then(({responseJson, status}) => {
            if (status === 200) {
                if (responseJson.status === 'OK') {
                    setRandomRecipes(responseJson.data)
                } else {
                    alert(responseJson.errorText)
                }
            } else {
                alert('status:' + status)
            }
        }).catch((reason => alert(reason)))
    }, []);

    return <div>
        {"Рацион на сегодня"}
        <br/>
        {"Последние добавленные продукты/рецепты"}
        <br/>
        {"Предложение давно неиспользуемых рецептов"}
        <h4>Случайные рецепты</h4>
        <hr/>
        {randomRecipes.length > 0 ? <div style={{alignContent: 'center'}}>
                {randomRecipes.map((e) => <Card style={{width: '18rem', margin: '10px', display: 'inline-block'}}>
                    <Card.Img variant="top" src={BACKEND_URL + '/recipe/images?name=' + e.imageName}/>
                    <Card.Body>
                        <Card.Title>{e.name}</Card.Title>
                        <Card.Text>{e.description}</Card.Text>
                        <Button variant="primary" onClick={() => {
                            history.push('/recipe/' + e.id)
                        }}>Открыть рецепт</Button>
                    </Card.Body>
                </Card>)}
            </div>
            : <div>Система не имеет сохраненных рецептов</div>
        }
    </div>
}