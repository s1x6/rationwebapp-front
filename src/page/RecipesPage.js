import React, {useEffect} from "react";
import RecipesList from "../components/recipe/RecipesList";

export default function (props) {

    useEffect(() => {
        document.title = "Рецепты - Ration"
    }, []);

    return <div style={{marginBottom: '128px'}}>
        <RecipesList/>
    </div>
}