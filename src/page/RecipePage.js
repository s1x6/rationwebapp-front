import React, {useEffect, useState} from "react";
import {useParams} from "react-router";
import {fetchGet} from "../utils";
import './recipe.css'
import {BACKEND_URL} from "../constants";
import {Badge, Button, Card, CardBody, CardImg, CardSubtitle, Col, FormCheckbox, Row} from "shards-react";
import {ListGroup, Table} from "react-bootstrap";
import {RecipeAddIngredient, recipeDeepCopy, StepsEdit} from "../components/recipe/RecipeEdit";

export default function (params) {
    let {id} = useParams();

    const [recipe, setRecipe] = useState(undefined)
    const [isEditing, setIsEditing] = useState(false)
    const [showAddIngredient, setShowAddIngredient] = useState(false)
    const [editingRecipe, setEditingRecipe] = useState(undefined)

    useEffect(() => {
        document.title = "Рецепты - Ration"
        fetchGet('/recipe/' + id).then(({responseJson, status}) => {
            if (status === 200) {
                if (responseJson.status === 'OK') {
                    setRecipe(responseJson.data)
                    document.title = responseJson.data.name + ' | рецепты - Ration'
                } else {
                    alert(responseJson.errorText)
                }
            } else {
                alert('status:' + status)
            }
        }).catch((reason => alert(reason)))
    }, [])

    const onIngredientDeleteClick = (index) => {
        if (editingRecipe.ingredients.length > 1) {
            const copy = recipeDeepCopy(editingRecipe)
            copy.ingredients.splice(index, 1)
            setEditingRecipe(copy)
        } else {
            alert('Вы не можете удалить последний ингредиент')
        }
    }

    const renderIngredientsTable = (recipe) => recipe.ingredients.map((e, i) => <>
        <tr>
            <th>{e.name}</th>
            <th>{e.calories}</th>
            <th>{e.carbohydrates}</th>
            <th>{e.proteins}</th>
            <th>{e.fats}</th>
            <th>{e.amount}</th>
            {isEditing ? <th><Button
                onClick={() => onIngredientDeleteClick(i)}
                theme='danger'>
                -
            </Button></th> : null}
        </tr>
    </>)

    const onIngredientAdd = (o) => {
        const copy = recipeDeepCopy(editingRecipe)
        copy.ingredients = [...copy.ingredients, o]
        setEditingRecipe(copy)
        console.log(JSON.stringify(copy.ingredients))
        console.log(JSON.stringify(o))
    }

    return <div style={{marginBottom: '128px'}}>
        {
            recipe === undefined ? "Loading" : <div>
                <h3>{recipe.name}</h3>
                <hr/>
                <div style={{display: 'flex'}}>
                    <Card style={{maxWidth: "400px"}}>
                        <CardImg top src={BACKEND_URL + '/recipe/images?name=' + recipe.imageName}/>
                        <CardBody>
                            <CardSubtitle>{recipe.description}</CardSubtitle>
                            {recipe.tags.map((t) => <Badge
                                    style={{margin: '4px'}}
                                    outline
                                    theme='danger'>
                                    {t.name}
                                </Badge>
                            )}
                        </CardBody>
                    </Card>
                    <Card style={{marginLeft: '16px', width: "100%"}}>
                        <CardBody>
                            <div style={{display: 'flex'}}>
                                <h4>Характеристики</h4>
                                <div style={{marginLeft: 'auto'}}>
                                    {recipe.isEditable ? <FormCheckbox
                                            checked={isEditing}
                                            toggle
                                            small
                                            onChange={() => {
                                                setEditingRecipe(recipeDeepCopy(recipe))
                                                console.log(JSON.stringify(recipeDeepCopy(recipe)))
                                                setIsEditing(!isEditing)
                                            }}>Редактирование</FormCheckbox> :
                                        <div>Рецепт добавлен не Вами</div>
                                    }
                                </div>
                            </div>
                            <div style={{display: 'flex', marginBottom: '16px'}}>
                                <Badge style={{marginRight: '4px', fontSize: '18px'}} outline theme='dark'>
                                    {'Общий вес: ' + recipe.totalWeight}
                                </Badge>
                                <Badge style={{fontSize: '18px'}} outline theme='dark'>
                                    {'Вес порции: ' + recipe.portionSize}
                                </Badge>
                            </div>
                            <Row style={{marginTop: '8px'}}>
                                <Col><b>Всего:</b></Col>
                                <Col><b>Калории: </b><i>{recipe.nutrientsTotal.calories.toFixed(1)}</i></Col>
                                <Col><b>Углеводы: </b><i>{+recipe.nutrientsTotal.carbohydrates.toFixed(1)}</i></Col>
                                <Col><b>Белки: </b><i>{+recipe.nutrientsTotal.proteins.toFixed(1)}</i></Col>
                                <Col><b>Жиры: </b><i>{+recipe.nutrientsTotal.fats.toFixed(1)}</i></Col>
                            </Row>
                            <Row style={{marginTop: '8px'}}>
                                <Col><b>На 100 грамм:</b></Col>
                                <Col><b>Калории: </b><i>{recipe.nutrientsDefault.calories.toFixed(1)}</i></Col>
                                <Col><b>Углеводы: </b><i>{recipe.nutrientsDefault.carbohydrates.toFixed(1)}</i></Col>
                                <Col><b>Белки: </b><i>{recipe.nutrientsDefault.proteins.toFixed(1)}</i></Col>
                                <Col><b>Жиры: </b><i>{recipe.nutrientsDefault.fats.toFixed(1)}</i></Col>
                            </Row>
                            <Row style={{marginTop: '8px'}}>
                                <Col><b>На порцию:</b></Col>
                                <Col><b>Калории: </b><i>{recipe.nutrientsPortion.calories.toFixed(1)}</i></Col>
                                <Col><b>Углеводы: </b><i>{recipe.nutrientsPortion.carbohydrates.toFixed(1)}</i></Col>
                                <Col><b>Белки: </b><i>{recipe.nutrientsPortion.proteins.toFixed(1)}</i></Col>
                                <Col><b>Жиры: </b><i>{recipe.nutrientsPortion.fats.toFixed(1)}</i></Col>
                            </Row>
                        </CardBody>
                    </Card>
                </div>
                <Card style={{marginTop: '16px', width: "100%"}}>
                    <CardBody>
                        <div style={{display: 'flex'}}>
                            <div style={{width: '50%'}}>
                                <h4>Шаги приготовления</h4>
                                {isEditing ? <StepsEdit setRecipe={setEditingRecipe} recipe={editingRecipe}/> :
                                    <ListGroup style={{textAlign: 'left'}} variant="flush">
                                        {recipe.steps.map((s) => <ListGroup.Item>
                                            <b>{s.num}.</b>{' ' + s.text}</ListGroup.Item>)}
                                    </ListGroup>
                                }
                            </div>
                            <div style={{width: '50%'}}>
                                <div style={{display: 'flex'}}>
                                    <h4>Ингредиенты</h4>
                                    <div style={{marginLeft: 'auto'}}>
                                        {isEditing ?
                                            <Button size='sm' outline onClick={() => setShowAddIngredient(true)}>
                                                Добавить
                                            </Button> : <Button size='sm' outline>Создать список</Button>}
                                    </div>
                                </div>
                                <Table style={{width: '100%'}} striped hover>
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>К</th>
                                        <th>У</th>
                                        <th>Б</th>
                                        <th>Ж</th>
                                        <th>В</th>
                                        {isEditing ? <th>Д</th> : null}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {isEditing ? renderIngredientsTable(editingRecipe) : renderIngredientsTable(recipe)}
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </CardBody>
                </Card>
            </div>
        }
        <RecipeAddIngredient onAdd={onIngredientAdd} show={showAddIngredient} onHide={() => {setShowAddIngredient(false)}}/>
    </div>
}