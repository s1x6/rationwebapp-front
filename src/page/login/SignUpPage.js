import React, {useEffect, useState} from "react";
import {Button, Form, FormGroup, FormInput} from "shards-react";
import {Link, useHistory} from "react-router-dom";
import {fetchLogin} from "../../utils";

export default function () {
    let history = useHistory();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [login, setLogin] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [name, setName] = useState("");

    useEffect(() => {
        document.title = " Регистрация - Ration"
    }, []);

    function handleEmailChange(e) {
        setEmail(e.target.value);
    }

    function handlePasswordChange(e) {
        setPassword(e.target.value);
    }

    function handleLoginChange(e) {
        setLogin(e.target.value);
    }

    function handlePasswordRepeatChange(e) {
        setRepeatPassword(e.target.value);
    }

    function handleNameChange(e) {
        setName(e.target.value);
    }

    function onSignUp() {
        if (password !== "" && name !== "" && email !== "" && login !== "" && repeatPassword !== "") {
            if (password === repeatPassword) {
                fetchLogin('/sign-up', JSON.stringify({
                    login: login,
                    password: password,
                    email: email,
                    name: name
                })).then((data) => {
                    if (data.responseJson.status === "OK") {
                        history.push("/login")
                    } else {
                        alert(data.responseJson.errorText)
                    }
                }).catch((reason => {
                    console.log(reason)
                }));
            } else {
                alert("Пароли не совпадают")
            }
        } else {
            alert("Вы заполнили не все поля")
        }
    }

    return <div className='login-form'>
        <div className='login-form-container'>
            <Form>
                <h3>Регистрация</h3>
                <FormGroup>
                    <label>Логин</label>
                    <FormInput id="#login" onChange={handleLoginChange} value={login}
                               placeholder="e.g. superNagibator2000"/>
                </FormGroup>
                <FormGroup>
                    <label htmlFor="#name">Имя</label>
                    <FormInput type="name" onChange={handleNameChange} value={name}
                               id="#name"
                               placeholder="Иван"/>
                </FormGroup>
                <FormGroup>
                    <label htmlFor="#email">Email</label>
                    <FormInput id="#email" onChange={handleEmailChange} value={email}
                               placeholder="example@email.com"/>
                </FormGroup>
                <FormGroup>
                    <label htmlFor="#password">Пароль</label>
                    <FormInput type="password" onChange={handlePasswordChange} value={password} id="#password"
                               placeholder="Пароль"/>
                </FormGroup>
                <FormGroup>
                    <label htmlFor="#repeatPassword">Повторите пароль</label>
                    <FormInput type="password" onChange={handlePasswordRepeatChange} value={repeatPassword}
                               id="#repeatPassword"
                               placeholder="Еще раз пароль"/>
                </FormGroup>
                <Button onClick={onSignUp}>Зарегистрироваться</Button>
                <div>
                    <p>Уже зарегистрированы? <Link to='/login'>Войти</Link></p>
                </div>
            </Form>
        </div>
    </div>
}