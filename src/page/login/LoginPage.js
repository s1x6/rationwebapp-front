import React, {useEffect, useState} from 'react'
import {Button, Form, FormGroup, FormInput} from "shards-react";
import './login.css'
import {Link, useHistory} from "react-router-dom";
import {fetchLogin} from '../../utils'
import Cookies from "universal-cookie";
import {BACKEND_URL} from "../../constants";

export default function () {
    let history = useHistory();
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    useEffect(() => {
        document.title = " Вход - Ration"
    }, []);

    function onLogin() {
        console.log(process.env.REACT_APP_BACKEND_URL)
        console.log(BACKEND_URL)
        fetchLogin('/login', JSON.stringify({
            login: login,
            password: password
        })).then((data) => {
            if (data.status === 403) {
                alert("Неверный пользователь и/или пароль");
                return
            }
            if (data.responseJson.status === "OK") {
                new Cookies().set('authToken', data.responseJson.data);
                history.push("/")
            } else {
                alert(data.responseJson.errorText)
            }
        }).catch((reason => {
            console.log(reason)
        }));
    }

    function handleLoginChange(e) {
        setLogin(e.target.value);
    }

    function handlePasswordChange(e) {
        setPassword(e.target.value);
    }

    return <div className='login-form'>
        <div className='login-form-container'>
            <Form>
                <h3>Вход</h3>
                <FormGroup>
                    <label htmlFor="#email">Логин</label>
                    <FormInput id="#email" onChange={handleLoginChange} value={login}
                               placeholder="username"/>
                </FormGroup>
                <FormGroup>
                    <label htmlFor="#password">Пароль</label>
                    <FormInput type="password" onChange={handlePasswordChange} value={password} id="#password"
                               placeholder="Пароль"/>
                </FormGroup>
                <Button onClick={onLogin}>Войти</Button>
                <div>
                    <p>Впервые на сайте? <Link to='/sign-up'>Зарегистрироваться</Link></p>
                </div>
            </Form>
        </div>
    </div>
}