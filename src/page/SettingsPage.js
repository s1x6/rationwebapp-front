import React, {useEffect} from "react";
import LogoutButton from "../components/LogoutButton";

export default function (props) {

    useEffect(() => {
        document.title = " Настройки - Ration"
    }, []);

    return <div>
        <LogoutButton/>
    </div>
}