import React, {useEffect, useState} from "react";
import DataTable from "react-data-table-component";
import {fetchGet} from "../utils";
import ProductInfo from "../components/product/ProductInfo";
import {Badge, Button, Card, CardBody, CardTitle} from "shards-react";
import ProductCreate from "../components/product/ProductCreate";

export default function (props) {

    const [data, setData] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [totalRows, setTotalRows] = useState(0)
    const [page, setPage] = useState(1)
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [isInfoShow, setIsInfoShow] = useState(false)
    const [isCreateShow, setIsCreateShow] = useState(false)
    const [isLoadingInfo, setIsLoadingInfo] = useState(false)
    const [loadedInfo, setLoadedInfo] = useState(false)

    const columns = [
        {
            name: 'Название',
            selector: 'name',
            sortable: true
        },
        {
            name: 'Калории (100г)',
            selector: 'calories',
            sortable: true
        },
        {
            name: 'Белки (100г)',
            selector: 'proteins',
            sortable: true
        },
        {
            name: 'Жиры (100г)',
            selector: 'fats',
            sortable: true
        },
        {
            name: 'Углеводы (100г)',
            selector: 'carbohydrates',
            sortable: true
        },
        {
            name: 'Источник',
            selector: 'isSystem',
            sortable: true,
            cell: row => <Badge
                outline
                theme={row.isSystem ? "primary" : "success"}>
                {row.isSystem ? "Система" : "Добавлено Вами"}
            </Badge>
        }
    ];

    useEffect(() => {
        document.title = "Продукты - Ration"
        loadTotal()
        load(page, rowsPerPage);
    }, []);


    function load(page, rowsPerPage) {
        fetchGet('/product?limit=' + rowsPerPage + '&page=' + (page - 1)).then(({responseJson, status}) => {
            if (status === 200) {
                setData(responseJson.data)
            } else {
                alert('status:' + status)
            }
        })
            .catch((reason => alert(reason)))
            .finally(() => {
                setIsLoading(false)
            })
    }

    function loadTotal() {
        fetchGet('/product/size').then(({responseJson, status}) => {
            if (status === 200) {
                setTotalRows(responseJson.data)
            } else {
                alert('status:' + status)
            }
        })
            .catch((reason => alert(reason)))
    }

    const onChangePage = (page) => {
        setIsLoading(true)
        setPage(page)
        load(page, rowsPerPage)
    }

    const onChangeRowsPerPage = (rows) => {
        setIsLoading(true)
        setRowsPerPage(rows)
        load(page, rows)
    }

    const onRowClick = (row) => {
        setIsLoadingInfo(true)
        fetchGet('/product/' + row.id).then(({responseJson, status}) => {
            if (status === 200) {
                setLoadedInfo(responseJson.data)
            } else {
                alert('status:' + status)
            }
        }).catch((reason => alert(reason)))
            .finally(() => {
                setIsLoadingInfo(false)
            })
        setIsInfoShow(true)
    }

    const handleInfoClose = () => {
        setIsInfoShow(false)
    }

    const handleCreateClose = () => {
        setIsCreateShow(false)
    }

    const onCreateClicked = () => {
        setIsCreateShow(true)
    }

    return <div>
        <Card style={{marginTop: '16px', width: "100%"}}>
            <CardBody>
                <CardTitle>Продукты</CardTitle>
                <DataTable
                    columns={columns}
                    data={data}
                    dense
                    direction={"ltr"}
                    actions={<Button outline onClick={onCreateClicked}>
                        Создать
                    </Button>}
                    progressPending={isLoading}
                    pagination
                    paginationServer
                    striped={true}
                    onChangePage={onChangePage}
                    onChangeRowsPerPage={onChangeRowsPerPage}
                    paginationTotalRows={totalRows}
                    highlightOnHover={true}
                    progressComponent={<h4>Загрузка...</h4>}
                    noDataComponent={<div>Продукты не найдены</div>}
                    onRowClicked={onRowClick}
                />
                <ProductInfo
                    show={isInfoShow}
                    handleClose={handleInfoClose}
                    product={loadedInfo}
                    isLoading={isLoadingInfo}
                    setProduct={(p) => setLoadedInfo(p)}
                />
                <ProductCreate
                    show={isCreateShow}
                    handleClose={handleCreateClose}
                    onCreate={() => {
                        loadTotal()
                        load(page, rowsPerPage);
                    }}
                />
            </CardBody>
        </Card>
    </div>
}